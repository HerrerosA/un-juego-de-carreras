# 3D - PEC1

## CÓMO JUGAR

Para jugar se iniciará en la escena "InicioJuego" y se podrá elegir entre los dos botones disponibles.
El primer botón nos llevará a una nueva carrera, y el segundo a ver la repetición de la última carrera realizada.
Los controles del teclado para dirigir el coche son los siguientes:
- Flecha hacia arriba: Acelerar
- Flecha hacia abajo: Frenar / Marcha atrás
- Flecha derecha: giro a la derecha de las ruedas delanteras
- Flecha izquierda: giro a la izquierda de las ruedas delanteras
Al iniciar una carrera los temporizadores estarán en 0 y cada vez que se cruce la línea de meta se pondrá en verde el temporizador vigente y en gris el anterior.
También se sumará 1 al número de vuelta en el que se encuentra la carrera.
Además cuando se termine una vuelta se mostrará el fantasma de la vuelta más rápida realizada durante esa carrera.
Tras finalizar las 3 vueltas el juego regresará al menú automáticamente.
El botón de la repetición nos mostrará desde distintas cámaras con diferentes ángulos toda la carrera realizada con anterioridad, las 3 vueltas. Al finalizar la reproducción volverá al menú automáticamente.


## ESTRUCTURA E IMPLEMENTACIÓN
En primer lugar se ha definido una escena inicial que permite elegir una u otra opción, esta escena tiene un script reproduce la escena de la carrera o la de la repetición.

# Escena carrera
En la escena de la carrera se ha diseñado un terreno con bastantes desniveles utilizando Terrain, se añadieron texturas (piedra, nieve, hierba), árboles y un lago. También se ha añadido viento para darle un efecto más real a la naturaleza.
Después se ha construido una carretera con EasyRoads3D y se ha pintado una línea de meta con un cubo de color blanco haciendo las veces de línea.
A lo largo de la carretera se han añadido diferentes puntos de control ordenados utilizando un cubo transparente que detecta la colisión del jugador mirando el tag, así evitando que se accionen con el coche fantasma.
Como los puntos de control están ordenados, si el jugador se salta uno o repite alguno de los anteriores porque da la vuelta, le aparecerá un aviso en el canvas que le indicará que no está siguiendo el camino correcto.
En el canvas además del aviso informativo de camino erróneo, también se han añadido la información de las vueltas y el tiempo de cada una de ellas, cambiando de color según su estado sea pendiente, finalizado o en curso.
Al elemento pista se le ha añadido un script de control de pista que lo que hace es contar el tiempo y las vueltas realizadas, así como los puntos de control atravesados por el jugador.
Al coche se le ha añadido un script que comprueba cuántas ruedas tiene haciendo colisión con el elemento que tiene el tag carretera, en función de cuántas no estén en contacto con esta, pero estén tocando algo, no cuando están en el aire, se aplica un multiplicador al drag, consiguiendo que el coche pierda velocidad pues gana mucho rozamiento.
Se ha añadido el elemento del coche fantasma que lo controla el script Ghost Manager situado en el elemento pista, que lo que hace es recoger muestras desde que pasa la primera vez por línea de meta, de manera muy seguida de la posición y rotación del jugador y las va guardando todas mientras dure la carrera. También guarda cuál es el tiempo de la vuelta más rápida para poder saber cuánto durará la siguiente reproducción.
Además guarda en qué muestra empieza cada vuelta, haciendo que una vez el jugador pasa por la línea de meta pueda empezar a reproducir el coche fantasma de la vuelta más rápida desde la muestra indicada. También seguirá grabando mientras se siga con la carrera aunque esté reproduciendo un fantasma.
Todos los datos de la posición del jugador de la carrera se guardan en un fichero Json.
Cuando se terminan las 3 vueltas se carga la escena de inicio del juego.

# Escena repetición
Se ha duplicado la escena de la carrera sin el jugador y sin la cámara y se ha colocado sólo el coche fantasma. Este coche tiene 4 cámaras con diferentes ángulos que tienen un temporizador y un random que elige cuál activar y desactiva el resto.
Cuando se elige reproducir la repetición se recoge los datos del fichero Json guardado en la escena de la carrera y se reproducen las 3 vueltas al completo. Se han modificado los scripts de GhostManager y Control de pista para que realice las acciones necesarias para reproducir y detener la reproducción en el momento necesario.
Cuando se llega al final de la reproducción se vuelve a cargar la escena de inicio del juego.