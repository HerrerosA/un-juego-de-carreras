﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuntosControlUI : MonoBehaviour
{
    [SerializeField] private ControlPista puntosControlPista;
    // Se muestra u oculta el UI que indica que no se ha pasado por el punto de control correcto según la situación que corresponda
    private void Start(){
        puntosControlPista.JugadorPuntoControlCorrecto += ControlesPista_JugadorPuntoControlCorrecto;
        puntosControlPista.JugadorPuntoControlErroneo += ControlesPista_JugadorPuntoControlErroneo;
        Ocultar();
    }
    private void ControlesPista_JugadorPuntoControlErroneo(object sender, System.EventArgs e){
        Mostrar();
    }
    private void ControlesPista_JugadorPuntoControlCorrecto(object sender, System.EventArgs e){
        Ocultar();
    }
    // Para mostrar u ocultar se pone como activo o no el objeto
    private void Mostrar(){
        gameObject.SetActive(true);
    }
    private void Ocultar(){
        gameObject.SetActive(false);
    }

}
