﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class TocaCarretera : MonoBehaviour
{
    public WheelCollider delateraDerecha;
    public WheelCollider delanteraIzquierda;
    public WheelCollider traseraDerecha;
    public WheelCollider traseraIzquierda;
    private Rigidbody coche;
    private int ruedasFuera;
    private float resistencia = 0.1f;

    private void Start(){
        // Se coge el rigid body del coche
        coche = this.GetComponent<Rigidbody>();
    }
    private void Update(){
        // Se define cada rueda para comprobar dónde se encuentra
        WheelHit toqueRuedaDelanteraDerecha;
        WheelHit toqueRuedaDelanteraIzquierda;
        WheelHit toqueRuedaTraseraDerecha;
        WheelHit toqueRuedaTraseraIzquierda;
        delateraDerecha.GetGroundHit(out toqueRuedaDelanteraDerecha);
        delanteraIzquierda.GetGroundHit(out toqueRuedaDelanteraIzquierda);
        traseraDerecha.GetGroundHit(out toqueRuedaTraseraDerecha);
        traseraIzquierda.GetGroundHit(out toqueRuedaTraseraIzquierda);
        // Se cuentan las ruedas que están fuera de la carretera
        if (toqueRuedaDelanteraDerecha.normal != Vector3.zero && toqueRuedaDelanteraIzquierda.normal != Vector3.zero && toqueRuedaTraseraDerecha.normal != Vector3.zero && toqueRuedaTraseraIzquierda.normal != Vector3.zero){
            ruedasFuera = 0;
            // Se compara el Tag de donde está tocando cada una para saber si es la carretera u otro
            if (!toqueRuedaDelanteraDerecha.collider.CompareTag("Carretera")){
                ruedasFuera+= 1;
            }
            if (!toqueRuedaDelanteraIzquierda.collider.CompareTag("Carretera")){
                ruedasFuera+= 1;
            } 
            if (!toqueRuedaTraseraDerecha.collider.CompareTag("Carretera")){
                ruedasFuera+= 1;
            } 
            if(!toqueRuedaTraseraIzquierda.collider.CompareTag("Carretera")){
                ruedasFuera+= 1;
            }
            // Por cada rueda que se encuentre fuera se aumenta el drag 0.15, lo que hace que el coche vaya más despacio
            coche.drag = resistencia + (ruedasFuera * 0.15f);
        }
    }
}
