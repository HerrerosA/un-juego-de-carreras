﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BotonesInicio : MonoBehaviour
{
    public void OnClickEmpezar(){
        SceneManager.LoadScene("Carrera");
    }
    public void OnClickRepetir(){
        SceneManager.LoadScene("Repetir");
    }
}
