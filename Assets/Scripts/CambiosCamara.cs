﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiosCamara : MonoBehaviour
{
    public Camera camara1;
    public Camera camara2;
    public Camera camara3;
    public Camera camara4;
    private float tiempoEntreCambios = 3.5f;
    private float tiempoTranscurrido;
    // Start is called before the first frame update
    void Start()
    {
        camara1.gameObject.SetActive(true);
        camara2.gameObject.SetActive(false);
        camara3.gameObject.SetActive(false);
        camara4.gameObject.SetActive(false);
        tiempoTranscurrido = 0.0f;
        
    }

    // Update is called once per frame
    void Update()
    {
        tiempoTranscurrido += Time.deltaTime;
        if (tiempoTranscurrido > tiempoEntreCambios){
            int a = Random.Range(0,3);
            switch(a)
            {
                case 0: 
                    camara1.gameObject.SetActive(true);
                    camara2.gameObject.SetActive(false);
                    camara3.gameObject.SetActive(false);
                    camara4.gameObject.SetActive(false);
                    break;
                case 1: 
                    camara2.gameObject.SetActive(true);
                    camara1.gameObject.SetActive(false);
                    camara3.gameObject.SetActive(false);
                    camara4.gameObject.SetActive(false);
                    break;
                case 2: 
                    camara3.gameObject.SetActive(true);
                    camara2.gameObject.SetActive(false);
                    camara1.gameObject.SetActive(false);
                    camara4.gameObject.SetActive(false);
                    break;
                case 3: 
                    camara4.gameObject.SetActive(true);
                    camara2.gameObject.SetActive(false);
                    camara3.gameObject.SetActive(false);
                    camara1.gameObject.SetActive(false);
                    break;

            }
            tiempoTranscurrido = 0.0f;
        }
        
    }
}
