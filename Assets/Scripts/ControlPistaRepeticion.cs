﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlPistaRepeticion : MonoBehaviour
{
    private bool reproduciendo = false;
    private GhostManagerRepeticion ghostManager;
    

    private void Awake(){
        ghostManager = this.GetComponent<GhostManagerRepeticion>();
        ghostManager.RepeticionCarrera();
        
    }
    
}
