﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlPista : MonoBehaviour
{
    public event EventHandler JugadorPuntoControlCorrecto;
    public event EventHandler JugadorPuntoControlErroneo;
    public Text vueltas;
    public Text tiempoVuelta1;
    public Text tiempoVuelta2;
    public Text tiempoVuelta3;
    private List<PuntoControl> listaPuntosControl;
    private int siguientePuntoControl;
    private int numeroVuelta;
    private float cronometro;
    private float mejorVuelta = 0.0f;
    private float tiempoReproduccion = 0.0f;
    private int inicioMejorVuelta = 0;
    private bool reproduciendo = false;
    private GhostManager ghostManager;
    private int inicioVuelta = 0;
    

    private void Awake(){
        // Se buscan todos los puntos de control que haya y se ponen en una lista
        ghostManager = this.GetComponent<GhostManager>();
        Transform puntosControlTransform = transform.Find("PuntosControl");
        listaPuntosControl = new List<PuntoControl>();
        foreach (Transform puntoControlTransform in puntosControlTransform){
            PuntoControl puntoControl = puntoControlTransform.GetComponent<PuntoControl>();
            puntoControl.EstablecerPuntosPista(this);
            listaPuntosControl.Add(puntoControl);
        }
        // Se establece que el primer punto de control es el que tiene índice 0
        siguientePuntoControl = 0;
        numeroVuelta = 0;
    }
    private void Update(){
        // Se van contando los tiempos de las vueltas
        cronometro = cronometro + Time.deltaTime;
        // Si se está reproduciendo se cuenta el tiempo que duró esa vuelta y luego se para
        if (reproduciendo){
            tiempoReproduccion = tiempoReproduccion + Time.deltaTime;
            if (tiempoReproduccion > mejorVuelta && numeroVuelta > 1){
                ghostManager.StopPlaying();
            }
        }
        if(numeroVuelta == 1){
            tiempoVuelta1.color = Color.green; 
            tiempoVuelta1.text = string.Format("{0:0}:{1:00}.{2:000}",
                     Mathf.Floor(cronometro / 60), //minutos
                     Mathf.Floor(cronometro) % 60, //segundos
                     Mathf.Floor((cronometro*1000) % 1000)); //milisegundos
        }
        if(numeroVuelta == 2){
            tiempoVuelta1.color = Color.gray;
            tiempoVuelta2.color = Color.green; 
            tiempoVuelta2.text = string.Format("{0:0}:{1:00}.{2:000}",
                     Mathf.Floor(cronometro / 60), //minutos
                     Mathf.Floor(cronometro) % 60, //segundos
                     Mathf.Floor((cronometro*1000) % 1000)); //milisegundos
        }
        if(numeroVuelta == 3){
            tiempoVuelta2.color = Color.gray;
            tiempoVuelta3.color = Color.green; 
            tiempoVuelta3.text = string.Format("{0:0}:{1:00}.{2:000}",
                     Mathf.Floor(cronometro / 60), //minutos
                     Mathf.Floor(cronometro) % 60, //segundos
                     Mathf.Floor((cronometro*1000) % 1000)); //milisegundos
        }
    }
    // Cada vez que el jugador atraviesa un punto de control se mira si es el que toca o no
    public void JugadorAtraviesaPunto(PuntoControl puntoControl){
        // Si el índice del punto de control que atraviesa corresponde al índice que indica el siguiente se suma uno a ese índice y se oculta el UI
        if (listaPuntosControl.IndexOf(puntoControl) == siguientePuntoControl){
            siguientePuntoControl = (siguientePuntoControl + 1) % listaPuntosControl.Count;
            JugadorPuntoControlCorrecto?.Invoke(this, EventArgs.Empty);
            if (siguientePuntoControl  == 1){
                // Si es la primera vuelta se empieza a grabar el coche fantasma
                if (numeroVuelta == 0){
                    ghostManager.StartRecording();
                    inicioMejorVuelta = inicioVuelta;
                }
                // Si es la última vuelta se termina de grabar
                if (numeroVuelta == 3){
                    ghostManager.StopRecording();
                    SceneManager.LoadScene("InicioJuego");

                }
                // Cada vez que pasa por la meta se suma 1 vuelta y se escribe en el UI
                numeroVuelta+= 1; 
                vueltas.text = numeroVuelta.ToString();
                // Si ya ha dado por lo menos una vuelta se guarda el tiempo de la mejor y en qué momento empieza la vuelta el fantasma
                if (numeroVuelta > 1){
                    if (cronometro < mejorVuelta || mejorVuelta == 0.0f){
                        mejorVuelta = cronometro;
                        inicioMejorVuelta = inicioVuelta;
                    }
                    // Se reproduce el fantasma desde el punto de la grabación del inicio de la mejor vuelta
                    ghostManager.currentSampleToPlay = inicioMejorVuelta;
                    ghostManager.StartPlaying();
                    // Se pone a 0 el tiempo de reproducción
                    tiempoReproduccion = 0.0f;
                    reproduciendo = true;
                    // Se guarda en qué muestra empieza la grabación de la vuelta actual
                    inicioVuelta = ghostManager.cantidadMuestras - 1;
                }
                // Se pone el cronómetro a 0 al pasar por la línea de meta
                cronometro = 0.0f;
            }
        }
        // Si no coincide el índice del punto de control que atraviesa con el índice que indica el siguiente se muestra el UI
        else{
            JugadorPuntoControlErroneo?.Invoke(this, EventArgs.Empty);
        }
        
    }
}
