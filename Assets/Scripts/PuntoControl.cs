﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

public class PuntoControl : MonoBehaviour
{
    private ControlPista controlPista;
    // Se comprueba que el CocheJugador atraviesa un punto de control
    private void OnTriggerEnter(Collider colision){
        if (colision.CompareTag("CocheJugador")){
            controlPista.JugadorAtraviesaPunto(this);
        }
    }
    public void EstablecerPuntosPista(ControlPista controlPista){
        this.controlPista = controlPista;
    }
}
