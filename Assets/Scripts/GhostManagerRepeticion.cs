﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;
using UnityEngine.SceneManagement;

public class GhostManagerRepeticion : MonoBehaviour
{
    public float timeBetweenSamples = 0.25f;
    public GhostLapData bestLapSO;              // Scriptable object that will contain the ghost data             
    public GameObject carToPlay;       
  

    // REPLAY VARIABLES
    private bool shouldPlay = false;
    public float totalPlayedTime = 0.0f;
    private float currenttimeBetweenPlaySamples = 0.0f;
    public int currentSampleToPlay = 0;

    // POSITIONS/ROTATIONS
    private Vector3 lastSamplePosition = Vector3.zero;
    private Quaternion lastSampleRotation = Quaternion.identity;
    private Vector3 nextPosition;
    private Quaternion nextRotation;


    #region PLAY GHOST DATA
    private void StartPlaying()
    {
        Debug.Log("START PLAYING");
        shouldPlay = true;
        carToPlay.gameObject.SetActive(true);
        // Tiene que poder grabar mientras reproduce la vuelta anterior
        //shouldRecord = false;

        // Seteamos los valores iniciales
        totalPlayedTime = 0;
       // Tenemos que elegir en qué muestra empieza a reproducir
       // currentSampleToPlay = 0;
        currenttimeBetweenPlaySamples = 0;

        // Desactivamos el control del coche
        carToPlay.GetComponent<CarController>().enabled = false;
        carToPlay.GetComponent<CarUserControl>().enabled = false;
    }

    public void StopPlaying()
    {
        Debug.Log("STOP PLAYING");
        shouldPlay = false;
        carToPlay.gameObject.SetActive(false);
        // Devolvemos el control al coche por si fuera necesario (opcional)
        carToPlay.GetComponent<CarController>().enabled = true;
        carToPlay.GetComponent<CarUserControl>().enabled = true;
        SceneManager.LoadScene("InicioJuego");

    }
    #endregion

    private void Update()
    {

        if (shouldPlay)
        {
            
            // A cada frame incrementamos el tiempo transcurrido 
            totalPlayedTime += Time.deltaTime;
            currenttimeBetweenPlaySamples += Time.deltaTime;

            // Si el tiempo transcurrido es mayor que el tiempo de muestreo
            if (currenttimeBetweenPlaySamples >= timeBetweenSamples)
            {
                // De cara a interpolar de una manera fluida la posición del coche entre una muestra y otra,
                // guardamos la posición y la rotación de la anterior muestra
                lastSamplePosition = nextPosition;
                lastSampleRotation = nextRotation;
                try{
                    // Cogemos los datos del scriptable object
                    bestLapSO.GetDataAt(currentSampleToPlay, out nextPosition, out nextRotation);

                    // Dejamos el tiempo extra entre una muestra y otra
                    currenttimeBetweenPlaySamples -= timeBetweenSamples;

                    // Incrementamos el contador de muestras
                    currentSampleToPlay++;
                    
                }
                catch{
                    StopPlaying();

                }
            }

            // De cara a crear una interpolación suave entre la posición y rotación entre una muestra y la otra, 
            // calculamos a nivel de tiempo entre muestras el porcentaje en el que nos encontramos
            float percentageBetweenFrames = currenttimeBetweenPlaySamples / timeBetweenSamples;
            Debug.Log(percentageBetweenFrames);

            // Aplicamos un lerp entre las posiciones y rotaciones de la muestra anterior y la siguiente según el procentaje actual.
            carToPlay.transform.position = Vector3.Slerp(lastSamplePosition, nextPosition, percentageBetweenFrames);
            carToPlay.transform.rotation = Quaternion.Slerp(lastSampleRotation, nextRotation, percentageBetweenFrames);
        }
    }


    public void RepeticionCarrera(){
        var archivoJson = Resources.Load<TextAsset>("registros_fantasma");
        JsonUtility.FromJsonOverwrite(archivoJson.text, bestLapSO);
        currentSampleToPlay = 0;
        StartPlaying();

    }

}

