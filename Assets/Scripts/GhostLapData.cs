﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[CreateAssetMenu]
[Serializable]
public class GhostLapData : ScriptableObject
{
    [SerializeField]List<Vector3> carPositions;
    [SerializeField]List<Quaternion> carRotations;

    public void AddNewData(Transform transform)
    {
        carPositions.Add(transform.position);
        carRotations.Add(transform.rotation);
    }

    public void GetDataAt(int sample, out Vector3 position, out Quaternion rotation)
    {
        position = carPositions[sample];
        rotation = carRotations[sample];
    }

    public void Reset()
    {
        Debug.Log("RESET");
        carPositions.Clear();
        carRotations.Clear();
    }

    public void SetData(List<Vector3> posiciones, List<Quaternion> rotaciones){
        carPositions = posiciones;
        carRotations = rotaciones;
    }
    public List<Vector3> getPositions(){
        return carPositions;
    }
    public List<Quaternion> getRotations(){
        return carRotations;
    }
}
